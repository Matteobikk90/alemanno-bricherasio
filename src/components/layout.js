/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.org/docs/use-static-query/
 */

import React from "react"
import { useStaticQuery, graphql } from "gatsby"
import Homepage from "./Homepage/homepage"
import ChiSiamo from "./ChiSiamo/chiSiamo"
import Servizi from "./Servizi/servizi"
import Contatti from "./Contatti/contatti"
import ScrollTop from "./ScrollTop/scrollTop"

import Grid from "./Lampadari/Grid/grid"

import "./layout.css"

const Layout = () => {
  const data = useStaticQuery(graphql`
    {
      allDataJson {
        edges {
          node {
            lampadari {
              image
              descrizione
              nome
              misura
              prezzo
              categoria
            }
          }
        }
      }
    }
  `)

  const { lampadari } = data.allDataJson.edges[0].node

  return (
    <>
      <main>
        <ScrollTop />
        <Homepage />
        <ChiSiamo />
        <Grid lampadari={lampadari} />
        <Servizi />
        <Contatti />
      </main>
    </>
  )
}

export default Layout
