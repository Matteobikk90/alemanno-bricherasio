import React from "react"

import GoogleApiWrapper from "./Maps/GoogleApiWrapper"

import chand from "../../images/chand.svg"
import insta from "../../images/insta.svg"
import phone from "../../images/phone.svg"
import email from "../../images/email.svg"
import face from "../../images/face.svg"
import wup from "../../images/wup.svg"

import "./contatti.css"
import FormContatti from "./FormContatti/formContatti"

const Contatti = () => {
  return (
    <section className="al_section5">
      <h2>Contatti</h2>
      <div className="contattiContainer">
        <div className="contatti">
          <div>
            <img src={chand} alt="Lampadario" />
            <p>
              Alemanno lampadari
              <br />
              Via Circonvallazione 76
              <br />
              10060 - Bricherasio, To
            </p>
          </div>
          <div className="cell">
            <a href="tel:+39012159150">
              <img src={phone} alt="Phone" />
              <p>0121 59150</p>
            </a>
            <div className="orari">
              <p>Lunedì: 15-19</p>
              <p>Martedì-Sabato: 9-12 / 15-19</p>
            </div>
          </div>
          <div>
            <a href="mailto:alemannolampadari@gmail.com">
              <img src={email} alt="Email" />
              <p>alemannolampadari@gmail.com</p>
            </a>
          </div>
          <div className="social">
            <a href="https://www.facebook.com/Alemanno-Lampadari-100223595007286">
              <img src={face} alt="Facebook" />
              <p>Facebook</p>
            </a>
            <a href="https://www.instagram.com/alemanno.lampadari/">
              <img src={insta} alt="Instagram" />
              <p>Instagram</p>
            </a>
            <a href="https://api.whatsapp.com/send?phone=+39335322489">
              <img src={wup} alt="Whatsapp" />
              <p>WhatsApp</p>
            </a>
          </div>
        </div>
        <div
          className="contatti"
          style={{ position: "relative", height: "50vh" }}
        >
          <GoogleApiWrapper />
        </div>
      </div>
      <FormContatti />
    </section>
  )
}

export default Contatti
