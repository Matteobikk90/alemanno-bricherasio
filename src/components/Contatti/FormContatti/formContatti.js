import React, { useState } from "react"

import "./formContatti.css"

const FormContatti = () => {
  const [data, setData] = useState({
    name: "",
    surname: "",
    email: "",
    phone: "",
    message: "",
    isChecked: false,
  })

  const handleChange = e => {
    const value =
      e.target.type === "checkbox" ? e.target.checked : e.target.value
    setData({
      ...data,
      [e.target.name]: value,
    })
  }

  const handleReset = () => {
    setData({
      name: "",
      surname: "",
      email: "",
      phone: "",
      message: "",
      isChecked: false,
    })
  }

  const handleSubmit = e => {
    e.preventDefault()
    if (data.isChecked) {
      const url = "https://alemanno.herokuapp.com/send"
      fetch(url, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
        },
        body: JSON.stringify(data),
      })
        .then(res => res.json())
        .then(data => {
          handleReset()
          alert("Messaggio inviato correttamente")
        })
        .catch(err => {
          alert("Messaggio non inviato")
        })
    } else {
      alert(
        "Per favore accettare i Termini e Condizioni e Privacy Policy prima di inviare il messaggio"
      )
    }
  }

  return (
    <form onSubmit={handleSubmit} id="contactForm">
      <h2>Scrivici</h2>
      <div>
        <label>
          <input
            type="text"
            required
            name="name"
            value={data.name}
            onChange={handleChange}
          />
          <span className="placeholder">Nome *</span>
        </label>
        <label>
          <input
            type="text"
            required
            name="surname"
            value={data.surname}
            onChange={handleChange}
          />
          <span className="placeholder">Cognome *</span>
        </label>
      </div>
      <div>
        <label>
          <input
            type="tel"
            required
            name="phone"
            value={data.phone}
            onChange={handleChange}
          />
          <span className="placeholder">Telefono *</span>
        </label>
        <label>
          <input
            type="email"
            required
            name="email"
            value={data.email}
            onChange={handleChange}
          />
          <span className="placeholder">Email *</span>
        </label>
      </div>
      <label>
        <textarea
          rows="8"
          required
          name="message"
          value={data.message}
          onChange={handleChange}
        ></textarea>
        <span className="placeholder">Messaggio *</span>
      </label>
      <p>
        <sup>*</sup> campi obbligatori
      </p>
      <div className="checkbox">
        <label>
          <span>Accettando, dichiari di aver letto i </span>
          <a
            target="_blank"
            href="https://agsoritilluminazione.com/terms&amp;conditions.html"
          >
            Termini e Condizioni
          </a>
          <span>e</span>
          <a href="https://agsoritilluminazione.com/privacy-policy.html">
            Privacy Policy
          </a>
          <input
            type="checkbox"
            name="isChecked"
            checked={data.isChecked}
            onChange={handleChange}
            id="agreeContact"
          />
          <span className="checkmark"></span>
        </label>
      </div>
      <input type="submit" value="Invia" />
    </form>
  )
}

export default FormContatti
