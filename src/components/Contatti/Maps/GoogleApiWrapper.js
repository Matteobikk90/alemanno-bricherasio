import React, { Component } from "react"
import { Map, Marker, GoogleApiWrapper } from "google-maps-react"

const mapStyles = {
  width: "100%",
  height: "500px",
  flex: 1,
}

export class MapContainer extends Component {
  render() {
    return (
      <Map
        google={this.props.google}
        zoom={17}
        style={mapStyles}
        initialCenter={{
          lat: 44.8159745,
          lng: 7.3001801,
        }}
      >
        <Marker
          name={"Alemanno Lampadari"}
          position={{ lat: 44.8159745, lng: 7.3001801 }}
        />
      </Map>
    )
  }
}

export default GoogleApiWrapper({
  apiKey: "AIzaSyB3LVVyXxIka4OUxUWfUAzogWMmZUtD9Ek",
})(MapContainer)
