import React, { useEffect } from "react"

import "./scrollTop.css"
import scrollTopImg from "../../images/scrollTop.svg"

const ScrollTop = () => {
  useEffect(() => {
    if (typeof window !== "undefined") {
      const scrollTop = document.querySelector("#scrollTopBtn")

      window.addEventListener("scroll", () => {
        toggleScrollTopBtn()
      })

      const toggleScrollTopBtn = () => {
        if (
          document.body.scrollTop > 400 ||
          document.documentElement.scrollTop > 400
        ) {
          scrollTop.classList.add("active")
        } else {
          scrollTop.classList.remove("active")
        }
      }
    }
  })

  const handleScrollToTop = () => {
    document.body.scrollTop = 0
    document.documentElement.scrollTop = 0
  }

  return (
    <img
      src={scrollTopImg}
      id="scrollTopBtn"
      onClick={handleScrollToTop}
      onKeyDown={handleScrollToTop}
      alt="Scroll to top"
    />
  )
}

export default ScrollTop
