import React from "react"

import "./chiSiamo.css"

const ChiSiamo = () => {
  return (
    <section className="al_section2">
      <h2>Chi Siamo</h2>
      <p>
        Dal 1967 qualità e tradizione. Rappresentiamo un punto di riferimento
        per l'acquisto di prodotti per l'illuminazione, un offerta in grado di
        soddisfare qualsiasi tipo di clientela con l'aiuto di un{" "}
        <span>architetto</span> in sede. Spaziamo dal moderno, al decorativo
        classico e ai prodotti per l'esterno. Ci impegnamo per darvi il meglio
        del Made in Italy a prezzi imbattibili! Durante l'anno organiziamo
        campagne promozionali con articoli in stock.
      </p>
    </section>
  )
}

export default ChiSiamo
