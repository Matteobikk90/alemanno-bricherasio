/**
 * SEO component that queries for data with
 *  Gatsby's useStaticQuery React hook
 *
 * See: https://www.gatsbyjs.org/docs/use-static-query/
 */

import React from "react"
import PropTypes from "prop-types"
import { Helmet } from "react-helmet"

function SEO(lang, meta) {
  return (
    <Helmet
      htmlAttributes={{
        lang,
      }}
      title="Alemanno Lampadari - Bricherasio"
      meta={[
        {
          name: `description`,
          content:
            "Dal 1967 qualità e tradizione. Rappresentiamo un punto di riferimento per l'acquisto di prodotti per l'illuminazione, un offerta in grado di soddisfare qualsiasi tipo di clientela con l'aiuto di un architetto in sede. Spaziamo dal moderno, al decorativo classico e ai prodotti per l'esterno.",
        },
        {
          property: `og:title`,
          content: "Alemanno Lampadari - Bricherasio",
        },
        {
          property: `og:description`,
          content:
            "Dal 1967 qualità e tradizione. Rappresentiamo un punto di riferimento per l'acquisto di prodotti per l'illuminazione, un offerta in grado di soddisfare qualsiasi tipo di clientela con l'aiuto di un architetto in sede. Spaziamo dal moderno, al decorativo classico e ai prodotti per l'esterno.",
        },
        {
          property: `og:type`,
          content: `website`,
        },
        {
          name: `twitter:card`,
          content: `summary`,
        },
        {
          name: `twitter:creator`,
          content: "Matteo Soresini",
        },
        {
          name: `twitter:title`,
          content: "Alemanno Lampadari - Bricherasio",
        },
        {
          name: `twitter:description`,
          content:
            "Dal 1967 qualità e tradizione. Rappresentiamo un punto di riferimento per l'acquisto di prodotti per l'illuminazione, un offerta in grado di soddisfare qualsiasi tipo di clientela con l'aiuto di un architetto in sede. Spaziamo dal moderno, al decorativo classico e ai prodotti per l'esterno.",
        },
      ].concat(meta)}
    />
  )
}

SEO.defaultProps = {
  lang: `en`,
  meta: [],
  description: ``,
}

SEO.propTypes = {
  description: PropTypes.string,
  lang: PropTypes.string,
  meta: PropTypes.arrayOf(PropTypes.object),
  title: PropTypes.string.isRequired,
}

export default SEO
