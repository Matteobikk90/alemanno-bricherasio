import React, { useState, useRef } from "react"

import Card from "../Card/card"

import "./grid.css"
import moderno from "../../../../public/static/immagini/moderno/mod1.jpg"
import classico from "../../../../public/static/immagini/classico/cla2.jpg"
import esterno from "../../../../public/static/immagini/esterno/est1.jpg"
import stock from "../../../../public/static/immagini/stock/sto1.jpg"

const Grid = props => {
  const { lampadari } = props
  const [filteredCategory, setFilteredCategory] = useState([])
  const scrollToRef = ref => window.scrollTo(0, ref.current.offsetTop)

  const handleCategory = category => {
    const filteredLampadari = lampadari.filter(
      lampadario => lampadario.categoria === category
    )
    setFilteredCategory(filteredLampadari)
    scrollToRef(myRef)
  }

  const myRef = useRef(null)

  return (
    <section className="al_section3">
      <h2>Lampadari</h2>
      <div>
        <article
          onKeyDown={() => handleCategory("moderno")}
          onClick={() => handleCategory("moderno")}
        >
          <h3>Moderno</h3>
          <img alt="Moderno" src={moderno} />
          <p>Scopri</p>
        </article>
        <article
          onKeyDown={() => handleCategory("classico")}
          onClick={() => handleCategory("classico")}
        >
          <h3>Classico</h3>
          <img alt="Classico" src={classico} />
          <p>Scopri</p>
        </article>
        <article
          onKeyDown={() => handleCategory("esterno")}
          onClick={() => handleCategory("esterno")}
        >
          <h3>Esterno</h3>
          <img alt="Esterno" src={esterno} />
          <p>Scopri</p>
        </article>
        <article
          onKeyDown={() => handleCategory("stock")}
          onClick={() => handleCategory("stock")}
        >
          <h3>Stock</h3>
          <img alt="Stock" src={stock} />
          <p>Scopri</p>
        </article>
      </div>
      {filteredCategory ? (
        <div className="subCategory" ref={myRef}>
          <Card filteredCategory={filteredCategory} />
        </div>
      ) : null}
    </section>
  )
}

export default Grid
