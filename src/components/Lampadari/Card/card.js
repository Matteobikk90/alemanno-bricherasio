import React from "react"

import "./card.css"

const Card = props => {
  const { filteredCategory } = props
  const lampadarioCard = filteredCategory.map((lampadario, i) => {
    return (
      <article className="cardProdotto" key={i}>
        <h3>{lampadario.nome}</h3>
        <img src={lampadario.image} alt={lampadario.nome} />
        <p>
          <span>Misura:</span> {lampadario.misura}
        </p>
        <p>
          <span>Info:</span> {lampadario.descrizione}
        </p>
        <p>
          <span>Prezzo:</span> {lampadario.prezzo}
        </p>
        <a
          href={`mailto:alemannolampadari@gmail.com?subject=${lampadario.nome}`}
        >
          Contattaci
        </a>
      </article>
    )
  })

  return lampadarioCard
}

export default Card
