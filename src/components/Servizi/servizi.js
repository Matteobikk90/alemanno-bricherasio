import React from "react"

import "./servizi.css"

const Servizi = () => {
  return (
    <section className="al_section4">
      <h2>Servizi</h2>
      <ul>
        <li>Consegna gratuita entro 30 km</li>
        <li>Consulenza architetto gratuita</li>
        <li>Prodotto in prova per reale ambientazione</li>
        <li>Supporto e-mail e telefonico</li>
      </ul>
    </section>
  )
}

export default Servizi
