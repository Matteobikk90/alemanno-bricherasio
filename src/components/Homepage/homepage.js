import React from "react"

import "./homepage.css"

const Homepage = () => {
  return (
    <section className="al_section1">
      <h1>
        <span>Alemanno</span>
        <span>Lampadari</span>
      </h1>
      <span className="line line1"></span>
      <span className="line line2"></span>
      <span className="line line3"></span>
      <span className="line line4"></span>
      <span className="line line5"></span>
      <span className="transitionLine"></span>
    </section>
  )
}

export default Homepage
